const loopImgs = () => {
    let wrapper = document.querySelector('.images-wrapper');
    let images = document.querySelectorAll('img');
    let stopButton = document.createElement('button');
    let continueButton = document.createElement('button');

    stopButton.innerText = 'Прекратить';
    continueButton.innerText = 'Возобновить';
    stopButton.style.marginBottom = '20px';
    wrapper.style.position = 'relative';
    document.body.prepend(stopButton);
    stopButton.after(continueButton);
    
    let currentIndex = 0;
    let numberOfImages = images.length;
    // let timer = document.createElement('span');
    // timerVal = 5;
    // timer.innerHTML = timerVal;
    // continueButton.after(timer);
    // let interval = 5 * 1000;

    // timer.innerHTML = timerVal--;

    // const showTimerFunc = () => {
    //     let showTimerVar = setInterval(() => {
    //         timer.innerHTML = timerVal--;
    //     }, 1000);
    // }

    // showTimerFunc();


    images.forEach(item => {
        item.style.cssText = "position: absolute; display: none; top: 0; left: 0";
    })

    images[0].style.display = 'block';

    let working = false;

        const changeImg = () => {
            working = true;
            images[currentIndex].style.display = 'none';

            if (currentIndex === images.length - 1) {
                currentIndex = 0;
            } else {
                currentIndex++;
            }
            images[currentIndex].style.display = 'block';
        };

    let changeImgStart = setInterval(changeImg, 2000);

        continueButton.addEventListener('click', () => {
            if(!working) changeImgStart = setInterval(changeImg, 2000);
        });

        stopButton.addEventListener('click', () => {
            clearInterval(changeImgStart);
            working = false;
        });
}

document.addEventListener('DOMContentLoaded', () => {loopImgs()});